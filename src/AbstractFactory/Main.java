package AbstractFactory;

public class Main {
	
	public static void main(String[] args) {
		Jasper jasper = new Jasper();
		jasper.whatImWearing();
		jasper.graduate();

		jasper.whatImWearing();
	}
}
