package AbstractFactory;

public interface Student {
	public void graduate();
}
