package AbstractFactory;

public class AdidasFactory implements ClothingFactory {

	public PieceOfClothing getShoes() {
		return new AdidasShoes();
	}

	public PieceOfClothing getPants() {
		return new AdidasJeans();
	}

	public PieceOfClothing getShirt() {
		return new AdidasShirt();
	}

	public PieceOfClothing getHat() {
		return new AdidasHat();
	}
}
