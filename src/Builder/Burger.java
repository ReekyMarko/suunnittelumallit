package Builder;

import java.util.ArrayList;

public class Burger {
	private String bun = "";
	private String patty = "";
	private String sauce = "";
	private String cheese = "";
	private ArrayList<String> ingredients;
	
	public Burger() {}
	public Burger(boolean list) {ingredients = new ArrayList<String>();};
	
	public void setBun(String bun) {this.bun = bun;}
	public void setPatty(String patty) {this.patty = patty;}
	public void setSauce(String sauce) {this.sauce = sauce;}
	public void setCheese(String cheese) {this.cheese = cheese;}
	
	public String getBun() {return bun;}	
	public String getPatty() {return patty;}	
	public String getSauce() {return sauce;}
	public String getCheese() {return cheese;}
	
	public ArrayList<String> getIngredients() {return ingredients;}
	public void addIngredient(String ingredient) {this.ingredients.add(ingredient);}
}
