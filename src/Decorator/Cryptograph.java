package Decorator;

import java.util.Scanner;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class Cryptograph extends EncryptedFileSaver {

  Scanner sc = new Scanner(System.in);

  public Cryptograph(SimpleFileSaver sfs) {
    super(sfs);
  }

  public void saveFile(byte[] text) {
    super.saveFile(encrypt(text));
  }

  public byte[] readFile() {
    return decrypt(super.readFile());
  }

  public byte[] encrypt(byte[] textIn) {
    readFile();
    System.out.println("Encrypting file");
    Cipher cipher;
    byte[] textEncrypted = null;

    try {
      System.out.print("Input your passphrase: ");
      String Key = sc.nextLine();
      byte[] KeyData = Key.getBytes();
      SecretKeySpec KS = new SecretKeySpec(KeyData, "Blowfish");

      cipher = Cipher.getInstance("Blowfish");

      cipher.init(Cipher.ENCRYPT_MODE, KS);
      textEncrypted = cipher.doFinal(textIn);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return textEncrypted;
  }

  public byte[] decrypt(byte[] textEncrypted) {
    System.out.println("Decrypting file");
    Cipher cipher;
    byte[] textDecrypted = null;

    try {
      System.out.print("Input your passphrase: ");
      String Key = sc.next();
      byte[] KeyData = Key.getBytes();
      SecretKeySpec KS = new SecretKeySpec(KeyData, "Blowfish");

      cipher = Cipher.getInstance("Blowfish");
      cipher.init(Cipher.DECRYPT_MODE, KS);

      textDecrypted = cipher.doFinal(textEncrypted);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return textDecrypted;
  }
}
