package Decorator;

import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		
		SimpleFileSaver sfs = new SimpleFileSaver();
		FileSaver cfs = new Cryptograph(sfs);
		
		String encrypt;
		String saveOrRead;
		String contents;
		
		while (true) {
			encrypt = null;
			saveOrRead = null;
			contents = null;
			
			System.out.print("Do you want to save or read a file? [s/r]: ");
			saveOrRead = sc.next();
			sc.nextLine();
			
			switch(saveOrRead) {
				case "s":	System.out.print("Type in the file contents: ");
							contents = sc.nextLine();
							System.out.print("Do you want the file to be encrypted? [y/n]: ");
							encrypt = sc.next();
							switch(encrypt) {
								case "n":	System.out.println(" ");
											sfs.saveFile(contents.getBytes());
											break;
								case "y":	System.out.println(" ");
											cfs.saveFile(contents.getBytes());
											break;
							}
							break;
				case "r": 	System.out.print("Is the file encrypted? [y/n]: ");
							encrypt = sc.next();
							switch(encrypt) {
								case "n": 	System.out.println(" ");
											contents = new String(sfs.readFile());
											System.out.println(" ");
											System.out.println("File contents: ");
											System.out.println(contents);
											break;
								case "y": 	System.out.println(" ");
											contents = new String(cfs.readFile());
											System.out.println(" ");
											System.out.println("File contents: ");
											System.out.println(contents);
											break;
							}
							break;
			}
			System.out.println(" ");
			System.out.print("Do you want to do another operation? [y/n]: ");
			if(sc.next().equals("n")) {
				break;
			}
			System.out.println(" ");
		}
	}
}
