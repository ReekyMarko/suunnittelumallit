package Decorator;

public abstract class EncryptedFileSaver implements FileSaver {
	SimpleFileSaver sfs;
	
	public EncryptedFileSaver(SimpleFileSaver sfs) {
		this.sfs = sfs;
	}
	
	public void saveFile(byte[] text) {
		sfs.saveFile(text);
	}
	
	public byte[] readFile() {
		return sfs.readFile();
	}
}
