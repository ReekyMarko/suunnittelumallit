package StateAndSingleton;

public interface State {
	public void evolve(final Pokemon pokemon);
	public void printStage();
}
