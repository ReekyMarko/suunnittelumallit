# Bridge
_Bridge-mallin idea on, että jokin yhdellä ohjelmistokerroksella toimiva abstraktio voidaan toteuttaa eri alustoilla ja että alusta voidaan vaikkapa vaihtaa kääntämättä asiakasohjelmaa. Valmistaudu kertomaan jokin esimerkki, jossa yhden kerroksen palvelu voidaan toteuttaa kahdella eri alustalla. Palauta kertomuksesi kirjallisessa muodossa Tuubiin._
---
Klassinen esimerkki Bridge-mallista on muotojen määrittely UI-ympäristössä. Hyvä konkreettinen esimerkki Javalla koodattuna tästä löytyy [Wikipediasta](https://en.wikipedia.org/wiki/Bridge_pattern#Java):

```java
/** "Implementor" */
interface DrawingAPI {
    public void drawCircle(final double x, final double y, final double radius);
}

/** "ConcreteImplementor"  1/2 */
class DrawingAPI1 implements DrawingAPI {
    public void drawCircle(final double x, final double y, final double radius) {
        System.out.printf("API1.circle at %f:%f radius %f\n", x, y, radius);
    }
}

/** "ConcreteImplementor" 2/2 */
class DrawingAPI2 implements DrawingAPI {
    public void drawCircle(final double x, final double y, final double radius) {
        System.out.printf("API2.circle at %f:%f radius %f\n", x, y, radius);
    }
}

/** "Abstraction" */
abstract class Shape {
    protected DrawingAPI drawingAPI;

    protected Shape(final DrawingAPI drawingAPI){
        this.drawingAPI = drawingAPI;
    }

    public abstract void draw();                                 // low-level
    public abstract void resizeByPercentage(final double pct);   // high-level
}

/** "Refined Abstraction" */
class CircleShape extends Shape {
    private double x, y, radius;
    public CircleShape(final double x, final double y, final double radius, final DrawingAPI drawingAPI) {
        super(drawingAPI);
        this.x = x;  this.y = y;  this.radius = radius;
    }

    // low-level i.e. Implementation specific
    public void draw() {
        drawingAPI.drawCircle(x, y, radius);
    }
    // high-level i.e. Abstraction specific
    public void resizeByPercentage(final double pct) {
        radius *= (1.0 + pct/100.0);
    }
}

/** "Client" */
class BridgePattern {
    public static void main(final String[] args) {
        Shape[] shapes = new Shape[] {
            new CircleShape(1, 2, 3, new DrawingAPI1()),
            new CircleShape(5, 7, 11, new DrawingAPI2())
        };

        for (Shape shape : shapes) {
            shape.resizeByPercentage(2.5);
            shape.draw();
        }
    }
}
```
Tämä ohjelma tulostaa:
```
API1.circle at 1.000000:2.000000 radius 3.075000
API2.circle at 5.000000:7.000000 radius 11.275000
```
Tässä esimerkissä on kaksi konkreettista implementaatiota piirtämisen toteuttavasta rajapinnasta DrawingAPI. Lisäksi on myös abstrakti luokka, jossa on määritelty  piirtämiseen tarvittavat metodit (Shape), ja sitä extendaava konkreettinen toteutus CircleShape. Tämä luokka ottaa konstruktoriinsa edellä mainitun DrawingAPI-rajapinnan konreettisen toteutuksen, joka määrittää miten ympyrä piirretään.

Täten luokka CircleShape on siis "silta", jonka avulla voi helposti vaihtaa alustaa.
