package Memento;

import java.util.Random;

public class Riddler {
	int high, low;
	
	public Riddler(int high, int low) {
		this.high = high;
		this.low = low;
	}
	
	public Memento joinGame() {
		Memento memento = new Memento(randomNumber());
		return memento;
	}
	
	private int randomNumber() {
		Random random = new Random();
		int number = random.nextInt(high) + low;
		return number;
	}
	
	public boolean guess(Memento memento, int guess, String name) {
		if(memento.getSavedState() == guess) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public static class Memento {
		private final int number;
		
		public Memento(int numberToSave) {
			number = numberToSave;
		}
		
		private int getSavedState() {
			return number;
		}
	}
}