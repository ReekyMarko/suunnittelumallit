package Memento;

import java.util.Random;

import Memento.Riddler.Memento;

public class Client implements Runnable {
	Riddler riddler;
	Memento memento;
	String name;
	boolean correct;
	int guesses, guess, high, low;
	
	public Client(Riddler riddler, String name, int high, int low) {
		this.riddler = riddler;
		this.name = name;
		this.high = high;
		this.low = low;
		memento = riddler.joinGame();
		guesses = 0;
	}
	
	private int randomNumber() {
		Random random = new Random();
		int number = random.nextInt(high) + low;
		return number;
	}
	
	private void guess() {
		int guess = randomNumber();
		correct = riddler.guess(memento, guess, name);
		guesses++;
		if(correct == true) {
			System.out.println(name+" guessed "+guess+" and they were right!");
			System.out.println(name+" got it right with "+guesses+" guesses");
			Thread.currentThread().interrupt();
		}
		else {
			System.out.println(name+" guessed "+guess+" and they were wrong");
		}
	}

	@Override
	public void run() {
		while(!Thread.interrupted()) {
			guess();
		}
	}
}
