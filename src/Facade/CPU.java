package Facade;

public class CPU {
	int position;
	public void jump(int position) {
		this.position = position;
	}
	
	public void execute(char[] data) {
		System.out.println(data[position]);
	}
}
