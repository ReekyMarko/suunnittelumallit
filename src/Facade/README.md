# Facade
Täydennä [Wikipediassa](https://en.wikipedia.org/wiki/Facade_pattern) olevaa Java-ohjelman runkoa tietokoneen ”boottaussimulaattorille”. Liitä ohjelmaan välivaiheiden tulostuksia. Ohjelman suorittaminen: tulosta muistiin ladatut datat (vaikkapa merkit, char) "muistiosoite" kerrallaan.
